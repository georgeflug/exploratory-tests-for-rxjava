import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.util.concurrent.CountDownLatch
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicReference

class SchedulerTests {

    @Test
    fun observeOn_AffectsTheLineRightBelowIt() {
        val scheduler1 = createSchedulerWithThreadName("scheduler1")
        val scheduler2 = createSchedulerWithThreadName("scheduler2")

        Observable.just("hello")
                .doOnNext { assertCurrentThreadIsNamed("main") }

                .observeOn(scheduler1)
                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }

                .observeOn(scheduler2)
                .doOnNext { assertCurrentThreadIsNamed("scheduler2") }

                .test()
                .await()
                .assertNoErrors()
    }

    @Test
    fun observeOn_AffectsAllLinesBelowIt_UntilObserveOnIsCalledAgain() {
        val scheduler1 = createSchedulerWithThreadName("scheduler1")
        val scheduler2 = createSchedulerWithThreadName("scheduler2")

        Observable.just("hello")
                .doOnNext { assertCurrentThreadIsNamed("main") }

                .observeOn(scheduler1)
                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }
                .map { text -> text.toUpperCase() }
                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }
                .map { text -> text.toLowerCase() }
                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }

                .observeOn(scheduler2)
                .doOnNext { assertCurrentThreadIsNamed("scheduler2") }

                .test()
                .await()
                .assertNoErrors()
    }

    @Test
    fun observeOn_UsesTheLastScheduler_WhenTwoAreInARow() {
        val scheduler1 = createSchedulerWithThreadName("scheduler1")
        val scheduler2 = createSchedulerWithThreadName("scheduler2")

        Observable.just("hello")
                .doOnNext { assertCurrentThreadIsNamed("main") }

                .observeOn(scheduler1)
                .observeOn(scheduler2)
                .doOnNext { assertCurrentThreadIsNamed("scheduler2") }

                .test()
                .await()
                .assertNoErrors()
    }

    @Test
    fun observeOn_AffectsTheSubscribeFunction() {
        val latch = CountDownLatch(1)
        val savedThreadName = AtomicReference<String>();
        val scheduler1 = createSchedulerWithThreadName("scheduler1")

        Observable.just("hello")
                .doOnNext { assertCurrentThreadIsNamed("main") }

                .observeOn(scheduler1)
                .subscribe { _ ->
                    savedThreadName.set(currentThreadName())
                    latch.countDown()
                }

        latch.await()
        assertThat(savedThreadName.get()).isEqualTo("scheduler1")
    }

    @Test
    fun subscribeOn_AffectsTheBeginningOfTheChain() {
        val scheduler1 = createSchedulerWithThreadName("scheduler1")

        Observable.just("hello")
                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }
                .subscribeOn(scheduler1)

                .test()
                .await()
                .assertNoErrors()
    }

    @Test
    fun subscribeOn_UsesTheFirstScheduler_WhenItWasCalledMoreThanOnce() {
        val scheduler1 = createSchedulerWithThreadName("scheduler1")
        val scheduler2 = createSchedulerWithThreadName("scheduler2")

        Observable.just("hello")
                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }
                .subscribeOn(scheduler1)
                .subscribeOn(scheduler2)

                .test()
                .await()
                .assertNoErrors()
    }

    @Test
    fun subscribeOn_AffectsTheSubscribeFunction_IfNoObserveOnIsPresent() {
        val latch = CountDownLatch(1)
        val savedThreadName = AtomicReference<String>();
        val scheduler1 = createSchedulerWithThreadName("scheduler1")

        Observable.just("hello")
                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }
                .subscribeOn(scheduler1)
                .doFinally { latch.countDown() }

                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }
                .map { text -> text.toUpperCase() }
                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }
                .map { text -> text.toLowerCase() }
                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }

                .subscribe { _ -> savedThreadName.set(currentThreadName()) }

        latch.await()
        assertThat(savedThreadName.get()).isEqualTo("scheduler1")
    }

    @Test
    fun subscribeOn_AffectsTheEntireChain_UntilObserveOnIsCalled() {
        val scheduler1 = createSchedulerWithThreadName("scheduler1")
        val scheduler2 = createSchedulerWithThreadName("scheduler2")

        Observable.just("hello")
                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }
                .subscribeOn(scheduler1)

                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }
                .map { text -> text.toUpperCase() }
                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }
                .map { text -> text.toLowerCase() }
                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }

                .observeOn(scheduler2)
                .doOnNext { assertCurrentThreadIsNamed("scheduler2") }

                .test()
                .await()
                .assertNoErrors()
    }

    @Test
    fun subscribeOn_DoesNotCareWhereInTheChainItIsCalled() {
        val scheduler1 = createSchedulerWithThreadName("scheduler1")
        val scheduler2 = createSchedulerWithThreadName("scheduler2")

        Observable.just("hello")
                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }
                .map { text -> text.toUpperCase() }
                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }
                .map { text -> text.toLowerCase() }
                .doOnNext { assertCurrentThreadIsNamed("scheduler1") }

                .observeOn(scheduler2)
                .doOnNext { assertCurrentThreadIsNamed("scheduler2") }

                .subscribeOn(scheduler1)
                .test()
                .await()
                .assertNoErrors()
    }

    @Test
    fun subscribeOn_AffectsCallablesThatSeedTheObservable() {
        val scheduler1 = createSchedulerWithThreadName("scheduler1")

        Observable.fromCallable {
            assertCurrentThreadIsNamed("scheduler1")
            "hello"
        }
                .subscribeOn(scheduler1)

                .test()
                .await()
                .assertNoErrors()
    }

    @Test
    fun subscribeOn_AffectsFinallyMethod_WhenTheObservableCompletesItself() {
        val latch = CountDownLatch(1)
        val savedThreadName = AtomicReference<String>();
        val scheduler1 = createSchedulerWithThreadName("scheduler1")

        Observable.just("hello")
                .subscribeOn(scheduler1)
                .doFinally { savedThreadName.set(currentThreadName()) }
                .doFinally { latch.countDown() }

                .test()
                .await()
                .assertNoErrors()

        latch.await()
        assertThat(savedThreadName.get()).isEqualTo("scheduler1")
    }

    @Test
    fun subscribeOn_DoesNotAffectFinallyMethod_WhenTheObservableIsManuallyDisposed() {
        val latch = CountDownLatch(1)
        val savedThreadName = AtomicReference<String>();
        val scheduler1 = createSchedulerWithThreadName("scheduler1")

        Observable.never<String>()
                .subscribeOn(scheduler1)
                .doFinally { savedThreadName.set(currentThreadName()) }
                .doFinally { latch.countDown() }

                .test()
                .dispose()

        latch.await()
        assertThat(savedThreadName.get()).isEqualTo("main")
    }

    @Test
    fun observeOn_AffectsFinallyMethod_WhenTheObservableCompletesItself() {
        val latch = CountDownLatch(1)
        val savedThreadName = AtomicReference<String>();
        val scheduler1 = createSchedulerWithThreadName("scheduler1")

        Observable.just("hello")
                .observeOn(scheduler1)
                .doFinally { savedThreadName.set(currentThreadName()) }
                .doFinally { latch.countDown() }

                .test()
                .await()
                .assertNoErrors()

        latch.await()
        assertThat(savedThreadName.get()).isEqualTo("scheduler1")
    }

    @Test
    fun observeOn_DoesNotAffectFinallyMethod_WhenTheObservableIsManuallyDisposed() {
        val latch = CountDownLatch(1)
        val savedThreadName = AtomicReference<String>();
        val scheduler1 = createSchedulerWithThreadName("scheduler1")

        Observable.never<String>()
                .observeOn(scheduler1)
                .doFinally { savedThreadName.set(currentThreadName()) }
                .doFinally { latch.countDown() }

                .test()
                .dispose()

        latch.await()
        assertThat(savedThreadName.get()).isEqualTo("main")
    }

    private fun createSchedulerWithThreadName(name: String): Scheduler {
        val threadFactory = { runnable: Runnable -> Thread(runnable, name) }
        val namedExecutor = Executors.newSingleThreadExecutor(threadFactory)
        return Schedulers.from(namedExecutor)
    }

    private fun assertCurrentThreadIsNamed(name: String) {
        assertThat(currentThreadName()).isEqualTo(name)
    }

    private fun currentThreadName() = Thread.currentThread().name;
}
